/**
 * Created with IntelliJ IDEA.
 * User: andy
 * Date: 03.09.13
 * Time: 12:19
 * To change this template use File | Settings | File Templates.
 */
package hock.enums {
public class ModelEvents {
  public static const MODIFY_CLUB:String = "modify_club";
  public static const MODIFY_PLAYER:String = "modify_player";
  public static const QUERY_EXECUTED:String = "QUERY_EXECUTED";
}
}
