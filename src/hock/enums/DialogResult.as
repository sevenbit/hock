/**
 * Created with IntelliJ IDEA.
 * User: andy
 * Date: 29.08.13
 * Time: 18:05
 * To change this template use File | Settings | File Templates.
 */
package hock.enums {
public class DialogResult {
  public static const OK:int = 0;
  public static const CANCEL:int = 1;
  public static const ADD_AND_CONTINUE:int = 2;
}
}
