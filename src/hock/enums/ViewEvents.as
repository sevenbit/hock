/**
 * Created with IntelliJ IDEA.
 * User: andy
 * Date: 29.08.13
 * Time: 15:16
 * To change this template use File | Settings | File Templates.
 */
package hock.enums {
public class ViewEvents {
  public static const CLUBS_TAB_ACTIVATED:String = "clubs_tab_activated";
  public static const PLAYERS_TAB_ACTIVATED:String = "playesr_tab_activated";
  public static const EDIT_DIALOG_CLOSED:String = "edit_dialog_closed";
  public static const EDIT_DIALOG_OPERATION_PERFORMED:String = "edit_dialog_operation_performed";
}
}
