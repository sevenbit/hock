/**
 * Created with IntelliJ IDEA.
 * User: andy
 * Date: 28.08.13
 * Time: 17:23
 * To change this template use File | Settings | File Templates.
 */
package hock.db {
import flash.data.SQLConnection;
import flash.data.SQLStatement;
import flash.events.EventDispatcher;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;

import hock.enums.ModelEvents;

import hock.utils.Logger;
import hock.view.common.ObjectEvent;

/**
 * Объект создается на каждый запрос к базе.
 * В конструктор передается уже созданное соединение
 */
public class
SqlQueryTask extends EventDispatcher{

  private var statement:SQLStatement;
  private var queryEnum:int;

  public function SqlQueryTask(connection:SQLConnection, text:String, queryEnum:int) {
    this.queryEnum = queryEnum;

    statement = new SQLStatement();
    statement.sqlConnection = connection;
    statement.text = text;
    statement.addEventListener(SQLErrorEvent.ERROR, onError);
    statement.addEventListener(SQLEvent.RESULT, onResult);
  }

  public function execute():void {
    statement.execute();
  }

  private function onError(e:SQLErrorEvent):void {
    Logger.info("sql statement error: " + e.toString());
    dispatchEvent(new ObjectEvent(ModelEvents.QUERY_EXECUTED, {result: "error", query: queryEnum}));
    removeListeners();
  }

  private function onResult(e:SQLEvent):void {
    Logger.info("query '" + statement.text + "' executed");
    var data:Array = statement.getResult().data;
    dispatchEvent(new ObjectEvent(ModelEvents.QUERY_EXECUTED, {result: "success", query:queryEnum, data: data}));
    removeListeners();
  }

  private function removeListeners():void{
    statement.removeEventListener(SQLErrorEvent.ERROR, onError);
    statement.removeEventListener(SQLEvent.RESULT, onResult);
    statement = null;
  }
}
}
