/**
 * User: andy
 * Date: 27.08.13
 * Time: 13:56
 */
package hock.db {
import flash.data.SQLConnection;
import flash.events.EventDispatcher;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;
import flash.filesystem.File;

import hock.enums.ModelEvents;
import hock.utils.Logger;
import hock.view.common.ObjectEvent;

public class SqliteDriver extends EventDispatcher implements ISqliteDriver {

  public static const DRIVER_EVENT:String = "sqlite_driver_event";

  private var connection:SQLConnection = new SQLConnection();


  public function start():void {
    var db:File = File.applicationDirectory.resolvePath("hockstat.sqlite");
    var dbExist:Boolean = db.exists;
    connection.openAsync(db);
    connection.addEventListener(SQLEvent.OPEN, db_opened);
    connection.addEventListener(SQLErrorEvent.ERROR, sqlc_error);
    if(!dbExist)
      createDB();
  }

  private function db_opened(e:SQLEvent):void {
    Logger.info("db opened");
  }

  private function sqlc_error(e:SQLErrorEvent):void {
    Logger.info("sql connection error: " + e.toString());
  }

  private function createAndExecuteTask(queryText:String, queryType:int):void {
    var task:SqlQueryTask = new SqlQueryTask(connection, queryText, queryType);
    task.addEventListener(ModelEvents.QUERY_EXECUTED, onQueryExecuted);
    task.execute();
  }

  public function getClubs():void {
    var query:String = "SELECT * from club";
    createAndExecuteTask(query, Query.GET_CLUBS);
  }

  public function getPlayers():void {
    var query:String = "select p.id, p.fio, p.club_id, p.goals, p.passes , p.role, c.name as club_name " +
                       "from player p " +
                       "inner join club c on p.club_id = c.id";
    createAndExecuteTask(query, Query.GET_PLAYERS);
  }

  public function addClub(name:String):void {
    var query:String = "insert into club(name) values ('" + name + "')";
    createAndExecuteTask(query, Query.ADD_CLUB);
  }

  public function addPlayer(fio:String, club_id:Number, goals:int, passes:int, role:String):void {
    var query:String = "insert into player(fio, club_id, goals, passes, role) values ("
            +   "'" + fio + "', "
            +   "'" + club_id + "', "
            +   "'" + goals + "', "
            +   "'" + passes + "', "
            +   "'" + role  + "')";
    createAndExecuteTask(query, Query.ADD_PLAYER);
  }

  public function removePlayer(id:Number):void {
    var query:String = "delete from player where id = '" + id + "'";
    createAndExecuteTask(query, Query.DELETE_PLAYER);
  }

  public function updatePlayer(id:Number, fio:String, club_id:Number, goals:int, passes:int, role:String):void {
    var query:String = "update player set "
      +   "fio    = '" + fio    + "', "
      +   "club_id= '" + club_id + "', "
      +   "goals  = '" + goals  + "', "
      +   "passes = '" + passes + "', "
      +   "role   = '" + role   + "' "
      +   "where id = '" + id + "'";
    createAndExecuteTask(query, Query.UPDATE_PLAYER);
  }

  public function createDB():void {
    var createClubTablesQuery:String =
      "CREATE TABLE club (id integer NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,name text NOT NULL UNIQUE) ";
    createAndExecuteTask(createClubTablesQuery, Query.CREATE_CLUB_TABLE);
    var createPlayerTablesQuery:String =
      "CREATE TABLE player (id integer NOT NULL PRIMARY KEY AUTOINCREMENT,fio text NOT NULL,\"club_id\" integer NOT NULL REFERENCES club (id) ON DELETE CASCADE,goals integer NOT NULL DEFAULT 0,passes integer NOT NULL DEFAULT 0, role text)";
    createAndExecuteTask(createPlayerTablesQuery, Query.CREATE_PLAYER_TABLE);

  }

  public function updateClub(id:Number, name:String):void {
    var query:String = "update club set name= '" + name + "' where id = '" + id + "'";
    createAndExecuteTask(query, Query.UPDATE_CLUB);
  }

  public function removeClub(clubId:Number):void {
    var query:String = "delete from player where club_id = '" + clubId + "'";
    createAndExecuteTask(query, Query.DELETE_ALL_CLUB_PLAYERS);
    query = "delete from club where id = '" + clubId + "'; ";
    createAndExecuteTask(query, Query.DELETE_CLUB);
  }

  private function onQueryExecuted(e:ObjectEvent):void {
    e.data.result == "error"
      ? processSqlError(e.data.query)
      : dispatchEvent(new ObjectEvent(DRIVER_EVENT, e.data));
  }

  private static function processSqlError(queryType:int):void {
    switch (queryType) {
      case Query.GET_CLUBS:
        Logger.error("Произошла ошибка при загрузке клубов");
        break;
      case Query.ADD_CLUB:
        Logger.error("Произошла ошибка при добавлении клуба");
        break;
      case Query.DELETE_CLUB:
        Logger.error("Произошла ошибка при удалении клуба");
        break;
      case Query.UPDATE_CLUB:
        Logger.error("Произошла ошибка при обновлении клуба");
        break;
      case Query.GET_PLAYERS:
        Logger.error("Произошла ошибка при загрузке игроков");
        break;
      case Query.ADD_PLAYER:
        Logger.error("Произошла ошибка при добавлении игрока");
        break;
      case Query.DELETE_PLAYER:
        Logger.error("Произошла ошибка при удалении игрока");
        break;
      case Query.UPDATE_PLAYER:
        Logger.error("Произошла ошибка при обновлении игрока");
        break;
      case Query.CREATE_CLUB_TABLE:
        Logger.error("Произошла ошибка при создани таблицы команд");
        break;
      case Query.CREATE_PLAYER_TABLE:
        Logger.error("Произошла ошибка при создании таблицы игроков");
        break;
      case Query.DELETE_ALL_CLUB_PLAYERS:
        Logger.error("Ошибка при удалении всех игроков клуба");
    }
  }

}
}
