/**
 * Created with IntelliJ IDEA.
 * User: andy
 * Date: 28.08.13
 * Time: 17:07
 * To change this template use File | Settings | File Templates.
 */
package hock.db {
public interface ISqliteDriver {
  function start():void;
  function getClubs():void;
  function getPlayers():void;
}
}
