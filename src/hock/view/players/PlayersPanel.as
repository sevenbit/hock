/**
 * User: andy
 * Date: 20.08.13
 * Time: 22:29
 */
package hock.view.players {
import flash.events.Event;
import flash.events.MouseEvent;

import hock.enums.DialogMode;
import hock.enums.DialogResult;
import hock.enums.ModelEvents;
import hock.enums.ViewEvents;
import hock.model.DataHelper;
import hock.utils.Logger;
import hock.view.common.DataPanel;
import hock.view.common.ObjectEvent;

import mx.collections.ArrayCollection;
import mx.collections.IList;
import mx.collections.ISort;
import mx.utils.StringUtil;

public class PlayersPanel extends DataPanel{

  private var _dialog:EditPlayerDialog;
  [Bindable]
  private var _clubs:ArrayCollection;

  override protected function addClick(event:MouseEvent):void {
    if(!_clubs || _clubs.length == 0) {
      Logger.error("Чтобы добавлять игроков, сначала добавьте хотя бы один клуб");
      return;
    }
    _dialog = EditPlayerDialog.createDialogForAdd(_clubs);
    _dialog.addEventListener(ViewEvents.EDIT_DIALOG_CLOSED, onDialogClosed);
    _dialog.addEventListener(ViewEvents.EDIT_DIALOG_OPERATION_PERFORMED, onAddAndContinue);
    _dialog.show();
  }

  override protected function updateClick(event:MouseEvent):void {
    _dialog = EditPlayerDialog.createDialogForEdit(grid.selectedItem, _clubs);
    _dialog.addEventListener(ViewEvents.EDIT_DIALOG_CLOSED, onDialogClosed);
    _dialog.show();
  }

  override protected function deleteClick(event:MouseEvent):void {
    dispatchEvent(new ObjectEvent(ModelEvents.MODIFY_PLAYER,
      {
        mode: DialogMode.DELETE,
        id:grid.selectedItem.id
      }
      , true, true));
  }


  private function onDialogClosed(event:ObjectEvent):void {
    _dialog.removeEventListener(ViewEvents.EDIT_DIALOG_CLOSED, onDialogClosed);
    _dialog.removeEventListener(ViewEvents.EDIT_DIALOG_OPERATION_PERFORMED, onAddAndContinue);
    if(event.data.result == DialogResult.CANCEL)
      return;
    dispatchEvent(new ObjectEvent(ModelEvents.MODIFY_PLAYER,
      {
        mode:     _dialog.dialogMode,
        fio:      StringUtil.trim(_dialog.fioInput.text),
        club_id:  _dialog.clubInput.selectedItem.id,
        id:       _dialog.objectToEditId,
        goals:    _dialog.goalsInput.value,
        passes:   _dialog.passesInput.value,
        role:     _dialog.roleInput.selectedItem
      }
      , true, true));
  }

  private function onAddAndContinue(event:Event):void {
    dispatchEvent(new ObjectEvent(ModelEvents.MODIFY_PLAYER,
      {
        mode:     _dialog.dialogMode,
        fio:      StringUtil.trim(_dialog.fioInput.text),
        club_id:  _dialog.clubInput.selectedItem.id,
        id:       _dialog.objectToEditId,
        goals:    _dialog.goalsInput.value,
        passes:   _dialog.passesInput.value,
        role:     _dialog.roleInput.selectedItem
      }
      , true, true));
  }

  override protected function getColumns():IList {
    return new PlayerColumns();
  }

  public function set clubs(value:ArrayCollection):void {
    _clubs = value;
    _clubs.sort = DataHelper.clubsSort;
    _clubs.refresh();
  }

  override protected function filterFunction(value:Object):Boolean {
    if(_filterText == "") return true;
    return value.fio.toLowerCase().indexOf(_filterText) != -1
        || value.club_name.toLowerCase().indexOf(_filterText) != -1
        || value.role.toLowerCase().indexOf(_filterText) != -1;
  }

  override protected function get sort():ISort {
    return DataHelper.playersSort;
  }
}
}
