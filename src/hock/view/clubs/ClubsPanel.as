/**
 * Created with IntelliJ IDEA.
 * User: andy
 * Date: 23.08.13
 * Time: 12:23
 * To change this template use File | Settings | File Templates.
 */
package hock.view.clubs {
import flash.events.MouseEvent;

import hock.enums.DialogMode;

import hock.enums.DialogResult;
import hock.enums.ModelEvents;
import hock.enums.ViewEvents;
import hock.model.DataHelper;
import hock.view.common.DataPanel;
import hock.view.common.ObjectEvent;

import mx.collections.IList;
import mx.collections.ISort;
import mx.controls.Alert;
import mx.events.CloseEvent;
import mx.utils.StringUtil;
import mx.utils.StringUtil;

import spark.collections.Sort;
import spark.collections.SortField;

public class ClubsPanel extends DataPanel{
  private var _dialog:EditClubDialog;

  override protected function addClick(event:MouseEvent):void {
    _dialog = EditClubDialog.createDialogForAdd();
    _dialog.addEventListener(ViewEvents.EDIT_DIALOG_CLOSED, onDialogClosed);
    _dialog.show();
  }

  override protected function updateClick(event:MouseEvent):void {
    _dialog = EditClubDialog.createDialogForEdit(grid.selectedItem);
    _dialog.addEventListener(ViewEvents.EDIT_DIALOG_CLOSED, onDialogClosed);
    _dialog.show();
  }

  override protected function deleteClick(event:MouseEvent):void {

    Alert.show(
      "Вы действительно хотите удалить клуб со всеми его игроками?",
      "Внимание",
      Alert.YES | Alert.NO,
      this,
      function (obj:CloseEvent):void {
        if(obj.detail == Alert.YES)
          dispatchEvent(new ObjectEvent(ModelEvents.MODIFY_CLUB,
            {
              mode: DialogMode.DELETE,
              id:grid.selectedItem.id
            }
            , true, true));
        },
      null,
      Alert.NO);
  }

  private function onDialogClosed(event:ObjectEvent):void {
    _dialog.removeEventListener(ViewEvents.EDIT_DIALOG_CLOSED, onDialogClosed);
    if(event.data.result == DialogResult.CANCEL)
      return;
    dispatchEvent(new ObjectEvent(ModelEvents.MODIFY_CLUB,
      {
        mode: _dialog.dialogMode,
        name: StringUtil.trim(_dialog.clubNameInput.text),
        id:   _dialog.objectToEditId
      }
      , true, true));
  }

  override protected function getColumns():IList {
    return new ClubsColumns();
  }

  override protected function filterFunction(value:Object):Boolean {
    if(_filterText == "") return true;
    return value.name.toLowerCase().indexOf(_filterText) != -1;
  }

  override protected function get sort():ISort {
    return DataHelper.clubsSort;
  }

}
}
