/**
 * Created with IntelliJ IDEA.
 * User: andy
 * Date: 26.08.13
 * Time: 16:35
 * To change this template use File | Settings | File Templates.
 */
package hock.view.common {
import avmplus.metadataXml;

import flash.events.Event;

public class ObjectEvent extends Event{
  public var data:Object;

  public function ObjectEvent(type:String, data: Object, bubbles:Boolean = false, cancelable:Boolean = false) {
    super(type, bubbles, cancelable);
    this.data = data;
  }
}
}
