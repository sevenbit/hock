/**
 * User: andy
 * Date: 23.08.13
 * Time: 12:22
 */
package hock.utils {
import hock.view.common.HockError;

public class CheckUtils {
  public static function checkTrue(value:Boolean, msg:String):void {
    if(value)
      throw new HockError(msg);
  }

  public static function checkFalse(value:Boolean, msg:String) :void {
    checkTrue(!value, msg);
  }
}
}
