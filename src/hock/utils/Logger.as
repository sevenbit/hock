/**
 * Created with IntelliJ IDEA.
 * User: andy
 * Date: 27.08.13
 * Time: 22:15
 * To change this template use File | Settings | File Templates.
 */
package hock.utils {
import mx.controls.Alert;

public class Logger {

  public static function info(msg:String) :void {
    trace(msg);
  }

  public static function error(msg:String):void {
    Alert.show(msg);
  }
}
}
