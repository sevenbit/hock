/**
 * Created with IntelliJ IDEA.
 * User: andy
 * Date: 27.08.13
 * Time: 22:29
 * To change this template use File | Settings | File Templates.
 */
package hock.model {
import flash.events.Event;

import hock.db.Query;
import hock.db.SqliteDriver;
import hock.enums.DialogMode;
import hock.enums.ModelEvents;
import hock.enums.ViewEvents;
import hock.view.common.ObjectEvent;

import mx.collections.ArrayCollection;

public class HockFacade {
  private var view:Hock;
  private var driver:SqliteDriver = new SqliteDriver();

  public function HockFacade(view:Hock) {
    this.view = view;
    driver.addEventListener(SqliteDriver.DRIVER_EVENT, onQueryExecuted);
    view.addEventListener(ViewEvents.CLUBS_TAB_ACTIVATED, onClubsTabActivated);
    view.addEventListener(ViewEvents.PLAYERS_TAB_ACTIVATED, onPlayersTabActivated);
    view.addEventListener(ModelEvents.MODIFY_CLUB, onClubModifyInitiated);
    view.addEventListener(ModelEvents.MODIFY_PLAYER, onPlayerModifyInitiated);
    driver.start();
    driver.getClubs();
  }

  private function onQueryExecuted(event:ObjectEvent):void {
    switch (event.data.query) {
      case Query.GET_CLUBS:
        var rawData:Array = event.data.data as Array;
        if(view.clubsWidget) //при загрузке данных из конструктора виджет клубов еще не создан
          view.clubsWidget.dataProvider = new ArrayCollection(rawData);
        view.playersWidget.clubs = new ArrayCollection(rawData);//для редактирования игроков
        break;
      case Query.ADD_CLUB:
      case Query.DELETE_CLUB:
      case Query.UPDATE_CLUB:
        driver.getClubs();
        break;
      case Query.GET_PLAYERS:
        rawData = event.data.data as Array;
        view.playersWidget.dataProvider = new ArrayCollection(rawData);
        break;
      case Query.ADD_PLAYER:
      case Query.UPDATE_PLAYER:
      case Query.DELETE_PLAYER:
        driver.getPlayers();
        break;


    }

  }

  private function onPlayersTabActivated(event:Event):void {
    driver.getPlayers();
  }

  private function onClubsTabActivated(event:Event):void {
    driver.getClubs();
  }

  private function onClubModifyInitiated(event:ObjectEvent):void {
    switch (event.data.mode) {
      case DialogMode.ADD:
        driver.addClub(event.data.name);
        break;
      case DialogMode.EDIT:
        driver.updateClub(event.data.id, event.data.name);
        break;
      case DialogMode.DELETE:
        driver.removeClub(event.data.id);
        break;
    }
  }

  private function onPlayerModifyInitiated(event:ObjectEvent):void{
    switch (event.data.mode){
      case DialogMode.ADD:
        driver.addPlayer(event.data.fio,  event.data.club_id, event.data.goals, event.data.passes, event.data.role);
        break;
      case DialogMode.DELETE:
        driver.removePlayer(event.data.id);
        break;
      case DialogMode.EDIT:
        driver.updatePlayer(event.data.id, event.data.fio, event.data.club_id, event.data.goals, event.data.passes, event.data.role);
    }
  }
}
}
