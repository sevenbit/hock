/**
 * Created with IntelliJ IDEA.
 * User: andy
 * Date: 04.09.13
 * Time: 18:49
 * To change this template use File | Settings | File Templates.
 */
package hock.model {
import mx.collections.ISort;

import spark.collections.Sort;
import spark.collections.SortField;

public class DataHelper {
  private static var playersSortInstance:ISort;
  private static var clubsSortInstance:ISort;

  public static function get clubsSort():ISort {
    if (clubsSortInstance == null) {
      var dataSortField:SortField = new SortField();
      dataSortField.name = "name";
      dataSortField.numeric = false;
      clubsSortInstance = new Sort();
      clubsSortInstance.fields = [dataSortField];
    }
    return clubsSortInstance;
  }

  public static function get playersSort():ISort {
    if(playersSortInstance == null) {
      var clubField:SortField = new SortField("club_name");
      var fioField:SortField = new SortField("fio");

      playersSortInstance = new Sort();
      playersSort.fields = [clubField, fioField];
      return playersSort;

    }
    return playersSortInstance;
  }
}
}
